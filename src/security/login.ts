import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';
import { User } from '../entity';
import { IContext, ILogin } from '../types/types';

export function createToken(user: User) {
    return jwt.sign(
        {
            id: user.id,
            email: user.email,
            exp: Math.floor(Date.now() / 1000 + parseInt(process.env.JWT_EXP_IN_SECONDS)),
        },
        process.env.JWT_SECRET,
    );
}

function randomTimeout(maxTime: number) {
    return new Promise(resolve => setTimeout(resolve, Math.random() * maxTime));
}

export async function login({ email: emailRaw, password }: ILogin, context: IContext) {
    const email = emailRaw.toLowerCase().trim();
    const userRepository = context.connection.getRepository(User);
    const user = await userRepository.findOne({ email });

    if (!user) {
        await randomTimeout(15);
        throw Error('Invalid credentials.');
    }

    // validate password
    const valid = await bcrypt.compare(password, user.password);
    if (!valid) {
        await randomTimeout(15);
        throw Error('Invalid credentials.');
    }

    // create jwt
    const token = createToken(user);
    return { user, token };
}
