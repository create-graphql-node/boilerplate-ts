import { GoogleProfile } from '../../types/types';
import passport from 'passport';
import { Application, NextFunction, Request, Response } from 'express';
import { loginOrRegister } from './loginOrRegister';

const { Strategy } = require('passport-google-oauth20'); // there are no types available

const GOOGLE_CALLBACK_PATH = '/auth/google/callback';

export function registerGoogleAuth(app: Application) {
    passport.use(
        new Strategy(
            {
                clientID: process.env.GOOGLE_CLIENT_ID,
                clientSecret: process.env.GOOGLE_CLIENT_SECRET,
                callbackURL: process.env.BASE_URL + GOOGLE_CALLBACK_PATH,
            },
            async (
                accessToken: string,
                refreshToken: string,
                profile: GoogleProfile,
                done: Function,
            ) => {
                const user = {
                    firstName: profile.name.givenName,
                    lastName: profile.name.familyName,
                    email: profile.emails[0].value,
                    password: '',
                };

                const loggedInUser = await loginOrRegister(user);
                done(null, loggedInUser);
            },
        ),
    );

    app.get(
        GOOGLE_CALLBACK_PATH,
        passport.authenticate('google', { scope: ['profile', 'email'] }),
        (req: Request, res: Response, next: NextFunction) => res.json(req.user),
    );
}
