import Strategy, { OptionsFunctionCallback, VerifyDoneCallback } from 'passport-ldapauth';
import passport from 'passport';
import { IncomingMessage } from 'http';
import { loginOrRegister } from './loginOrRegister';

export function registerLDAPAuth() {
    passport.use(
        new Strategy(
            (req: IncomingMessage, callback: OptionsFunctionCallback) => {
                callback(null, {
                    server: {
                        url: process.env.LDAP_URL,
                        bindDN: process.env.LDAP_BIND_DN,
                        bindCredentials: process.env.LDAP_BIND_PASSWORD,
                        searchBase: process.env.LDAP_SEARCH_BASE,
                        searchFilter: process.env.LDAP_SEARCH_FILTER,
                    },
                });
            },
            async (user: any, done: VerifyDoneCallback) => {
                const loggedInUser = await loginOrRegister(user);
                done(null, loggedInUser);
            },
        ),
    );
}
