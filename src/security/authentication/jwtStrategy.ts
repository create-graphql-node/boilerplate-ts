import passport from 'passport';
import { Strategy as JwtStrategy, ExtractJwt } from 'passport-jwt';
import { User } from '../../entity';
import { Request } from 'express';

function fromCookie(req: Request) {
    let token = null;
    if (req && req.cookies) {
        token = req.cookies['jwt'];
    }
    return token;
}

const opts = {
    jwtFromRequest: ExtractJwt.fromExtractors([
        ExtractJwt.fromUrlQueryParameter('jwt'),
        ExtractJwt.fromAuthHeaderAsBearerToken(),
        fromCookie,
    ]),
    secretOrKey: process.env.JWT_SECRET,
};

export function registerJWTAuth() {
    passport.use(
        new JwtStrategy(opts, async (jwtPayload, done) => {
            const user = await User.findOne({ id: jwtPayload.id });

            if (user) {
                return done(null, user);
            }

            return done(null, false);
        }),
    );
}
