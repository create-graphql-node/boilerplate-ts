import { createToken } from '../login';
import { register } from '../register';
import { User } from '../../entity';

export async function loginOrRegister(
    profile: { firstName: string; lastName: string; email: string; password?: string },
) {
    const { firstName, lastName, email, password } = profile;
    const user = await User.findOne({ email });

    if (!user) {
        return register({
            firstName,
            lastName,
            email,
            password,
        });
    }

    return {
        user,
        token: createToken(user),
    };
}
