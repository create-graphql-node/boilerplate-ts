import { Application, NextFunction, Request, Response } from 'express';
import passport from 'passport';
import { registerJWTAuth } from './jwtStrategy';
import { registerGoogleAuth } from './googleStrategy';
import { User } from '../../entity';
import { registerLDAPAuth } from './ldapStrategy';

function resolveUser(req: Request, resolve: Function, strategy: string) {
    return (err: Error, user: User) => {
        if (user) {
            req.user = user;
        }

        resolve(user);
    };
}

const NOOP = (...args: any[]) => {};

export function createAuthentication(app: Application) {
    passport.serializeUser((user: User | { user: User; token: string }, done: Function) => {
        if (user instanceof User) {
            return done(null, user.id);
        }

        done(null, user.user.id);
    });

    passport.deserializeUser(async (id: string, done: Function) => {
        const user = await User.findOne(id);
        done(null, user);
    });

    registerJWTAuth();
    if (process.env.GOOGLE_CLIENT_ID) {
        registerGoogleAuth(app);
    }

    if (process.env.LDAP_URL) {
        registerLDAPAuth();
    }

    return async function authentication(req: Request, res: Response, next: NextFunction) {
        await new Promise(resolve => {
            passport.authenticate(
                'jwt',
                { session: true, failWithError: false },
                resolveUser(req, resolve, 'jwt'),
            )(req, res, NOOP);
        });

        if (process.env.LDAP_URL) {
            await new Promise(resolve => {
                passport.authenticate(
                    'ldapauth',
                    { session: true, failWithError: true },
                    resolveUser(req, resolve, 'ldapauth'),
                )(req, res, NOOP);
            });
        }
        next();
    };
}
