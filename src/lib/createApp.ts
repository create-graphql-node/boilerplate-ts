import express, { NextFunction, Response, Request } from 'express';
import cors from 'cors';
import compression from 'compression';
import voyagerMiddleware from 'graphql-voyager/middleware/express';
import passport from 'passport';
import { GraphQLContextCreator, IContext } from '../types/types';
import { User } from '../entity';

export function createApp() {
    const app = express();

    if (process.env.NODE_ENV === 'development') {
        app.use(cors());
    }

    app.use(compression());

    if (process.env.NODE_ENV !== 'production') {
        app.use(
            '/voyager/m',
            voyagerMiddleware({
                endpointUrl: '/graphql',
                displayOptions: { rootType: 'Mutation' },
            }),
        );

        app.use(
            '/voyager/q',
            voyagerMiddleware({
                endpointUrl: '/graphql',
                displayOptions: { rootType: 'Query' },
            }),
        );
    }

    app.use(passport.initialize());

    return app;
}

export function expressErrorHandler(err: any, req: Request, res: Response, next: NextFunction) {
    if (err! instanceof Error) {
        next();
    }

    console.error(`${new Date().getTime()} ${err}`);

    res.status(err.status || 500).json({
        message: err.message,
        name: err.name,
        status: err.status,
    });
}

export async function createContext({ req, connection }: GraphQLContextCreator): Promise<IContext> {
    let user: any = null;

    if (req.user) {
        user = await connection.getRepository(User).findOne(req.user.id);
    }

    return {
        connection,
        user,
        req,
    };
}
