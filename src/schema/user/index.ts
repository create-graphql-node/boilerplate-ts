import { Generator, IContext, ILogin, NestedResolver } from '../../types/types';
import { User } from '../../entity';
import { paginate, filter, compose, generateMutations, withName, just } from 'create-graphql-node';
import { IUserCreate, register } from '../../security/register';
import { login } from '../../security/login';
import { AuthorizationError } from '../../errors';

const { remove, update } = Generator.IBasicResolverNames;
const userResolver: NestedResolver = {
    Mutation: {
        ...compose(
            withName('User'),
            just([remove, update]),
        )(
            generateMutations(User, {
                hooks: {
                    preUpdate: (user, rootValue, args, context) => {
                        if (!user.hasRole(User.ROLES.ADMIN) && user.id !== context.user.id) {
                            throw new AuthorizationError();
                        }
                    },
                    preRemove: (user, rootValue, args, context) => {
                        if (!user.hasRole(User.ROLES.ADMIN) && user.id !== context.user.id) {
                            throw new AuthorizationError();
                        }
                    },
                },
            }),
        ),
        login: async (_: any, args: ILogin, context: IContext) => login(args, context),
        register: async (_: any, args: IUserCreate, context: IContext) => register(args, context),
    },
    Query: {
        allUsers: async (rootValue, args, context: IContext) => {
            const repository = context.connection.getRepository(User);
            const queryBuilder = filter(rootValue, args, {
                connection: context.connection,
                queryBuilder: repository.createQueryBuilder('base'),
            });

            return paginate(rootValue, args, {
                connection: context.connection,
                repository,
                queryBuilder,
            });
        },
        me: async (rootValue, args, context: IContext) => {
            return context.user;
        }
    },
    User: {
        posts: async (user: User) => user.getPosts()
    }
};

export default userResolver;
