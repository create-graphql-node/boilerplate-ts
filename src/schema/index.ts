import * as path from 'path';
import { addMockFunctionsToSchema, makeExecutableSchema } from 'graphql-tools';
import resolvers, { schemaDirectives } from './resolvers';
import { importSchema } from 'graphql-import';

export const schema = makeExecutableSchema({
    typeDefs: importSchema(path.join(__dirname, './schema.graphql')),
    resolvers,
    schemaDirectives,
    logger: console,
});

addMockFunctionsToSchema({
    schema,
    preserveResolvers: true,
    mocks: {
        DateTime: () => new Date(),
    },
});
