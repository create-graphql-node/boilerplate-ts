import { GraphQLDateTime } from 'graphql-iso-date';
import { CursorType } from './types/CursorType';

export default {
    DateTime: GraphQLDateTime,
    Cursor: CursorType,
};
