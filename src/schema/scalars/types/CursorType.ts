import { GraphQLScalarType } from 'graphql';
import { Kind } from 'graphql/language';

export const CursorType = new GraphQLScalarType({
    name: 'Cursor',
    serialize(id) {
        return id;
    },
    parseLiteral(ast) {
        if (ast.kind === Kind.STRING) {
            return ast.value;
        }
        return null;
    },
    parseValue(value) {
        return value;
    },
});
