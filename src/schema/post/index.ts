import { IContext, NestedResolver } from '../../types/types';
import { Post } from '../../entity';
import { paginate, filter, compose, generateMutations, withName } from 'create-graphql-node';

const postResolver: NestedResolver = {
    Query: {
        allPosts: async (rootValue, args, context: IContext, info) => {
            const repository = context.connection.getRepository(Post);
            const queryBuilder = filter(rootValue, args, {
                connection: context.connection,
                queryBuilder: repository.createQueryBuilder('base'),
            });

            return paginate(rootValue, args, {
                connection: context.connection,
                repository,
                queryBuilder,
            });
        },
    },
    Mutation: {
        ...compose(withName('Post'))(generateMutations(Post)),
    },
};

export default postResolver;
