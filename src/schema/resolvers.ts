import { merge } from 'lodash';
import scalarsResolver from './scalars';
import userResolver from './user';
import postResolver from './post';

let version = 'none';

try {
    const packagejson = require('../../package.json');
    version = packagejson.version;
} catch (e) {
    console.warn(`No package.json found. Version is set to '${version}'`);
}

export default merge(
    {
        Mutation: { ping: () => 'pong' },
        Query: { version: () => version },
        Node: {
            __resolveType: () => null,
        },
    },
    postResolver,
    scalarsResolver,
    userResolver,
);

export * from './directives';
