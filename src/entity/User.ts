import { Entity, Column, PrimaryGeneratedColumn, BaseEntity, OneToMany } from 'typeorm';
import { Post } from './Post';

@Entity()
export class User extends BaseEntity {
    static ROLE_DELIMITER = ',';
    static ROLES = { USER: 'USER', ANON: 'ANON', ADMIN: 'ADMIN' };

    @PrimaryGeneratedColumn('uuid') id: string;

    @Column({ type: 'varchar', length: 100 })
    firstName: string;

    @Column({ type: 'varchar', length: 100 })
    lastName: string;

    @Column({ type: 'varchar', length: 100 })
    email: string;

    @Column({ type: 'text' })
    roles: string;

    @Column({ type: 'varchar', length: 100, nullable: true })
    password: string;

    @OneToMany(() => Post, p => p.creator, {
        cascade: ['insert', 'remove'],
    })
    posts: Promise<Post[]>;

    getPosts() {
        if (!Array.isArray(this.posts)) {
            this.posts = Promise.resolve([]);
        }
        return this.posts;
    }

    async addPosts(postsInput: Post[]) {
        if (!Array.isArray(this.posts)) {
            this.posts = Promise.resolve([]);
        }

        const posts = postsInput.map(p => {
            const created = Post.create(p);
            created.creator = this;
            return created;
        });

        const loadedRelation = await this.posts;
        loadedRelation.push(...posts);
        this.posts = Promise.resolve(loadedRelation);
    }

    async removePosts(posts: Post[]) {
        this.posts = Promise.resolve(
            ((await this.posts) || []).filter(a => !!posts.find(b => a.id === b.id)),
        );
    }

    hasRole(role: string) {
        return this.roles.split(User.ROLE_DELIMITER).includes(role);
    }

    addRole(role: string) {
        if (this.hasRole(role)) {
            return this;
        }

        this.roles = [...this.roles.split(User.ROLE_DELIMITER), role].join(User.ROLE_DELIMITER);
        return this;
    }

    removeRole(role: string) {
        if (!this.hasRole(role)) {
            return this;
        }

        this.roles = this.roles
            .split(User.ROLE_DELIMITER)
            .filter(r => role !== r)
            .join(User.ROLE_DELIMITER);
        return this;
    }

    toJSON() {
        const { firstName, lastName, email, roles } = this;

        return {
            firstName,
            lastName,
            email,
            roles,
        };
    }
}
