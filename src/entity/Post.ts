import { Entity, Column, PrimaryGeneratedColumn, BaseEntity, ManyToOne } from 'typeorm';
import { User } from './User';

@Entity()
export class Post extends BaseEntity {
    @PrimaryGeneratedColumn('uuid') id: string;

    @Column({ type: 'text', nullable: true })
    content?: string;

    @ManyToOne(type => User, u => u.posts, { cascade: ['insert'] })
    creator: User;
}
