require('dotenv').config();
import 'reflect-metadata';
import { Request } from 'express';
import bodyParser from 'body-parser';
import { ApolloServer } from 'apollo-server-express';
import depthLimit from 'graphql-depth-limit';
import { schema } from './schema';
import { createConnection } from 'typeorm';
import { createApp, createContext, expressErrorHandler } from './lib/createApp';
import './security/authentication/jwtStrategy.ts';
import * as path from 'path';
import { createAuthentication } from './security/authentication';

const graphqlEndpoint = '/graphql';

async function main() {
    const app = createApp();

    const connection = await createConnection({
        type: 'postgres',
        host: process.env.TYPEORM_HOST,
        username: process.env.TYPEORM_USERNAME,
        password: process.env.TYPEORM_PASSWORD,
        database: process.env.TYPEORM_DATABASE,
        entities: [path.join(__dirname, 'entity/**')],
    });

    const server = new ApolloServer({
        context: async ({ req }: { req: Request }) => await createContext({ req, connection }),
        schema,
        validationRules: [depthLimit(10)],
        playground: process.env.NODE_ENV !== 'production' ? { endpoint: graphqlEndpoint } : false,
    });

    app.use(graphqlEndpoint, bodyParser.json(), createAuthentication(app));
    server.applyMiddleware({ app, path: graphqlEndpoint });

    app.use(expressErrorHandler);

    const expressServer = app.listen(process.env.PORT, () => {
        const address = expressServer.address();
        if (typeof address === 'object') {
            const { address: host, port } = address;
            console.log('🚀 Listening on %s:%s', host, port);
        }
    });
}

main().catch(e => {
    console.error(e);
    process.exit(1);
});
