import { Request } from 'express';
import { BaseEntity, Connection } from 'typeorm';
import { GraphQLResolveInfo } from 'graphql';
import { SelectQueryBuilder } from 'typeorm/query-builder/SelectQueryBuilder';
import { User } from '../entity';

export interface IObject {
    [key: string]: any;
}

export interface GraphQLContextCreator {
    req: Request;
    connection: Connection;
}

export interface IContext {
    connection: Connection;
    user?: User;
    req?: Request;
}

export type ResolverFn<T = any> = (
    rootValue: any,
    args: any,
    context: any,
    info: GraphQLResolveInfo,
) => Promise<T>;

export interface NestedResolver {
    [name: string]: ResolverFn | NestedResolver;
}

export namespace Query {
    export enum SortType {
        ASC = 'ASC',
        DESC = 'DESC',
    }

    export enum FilterOperator {
        eq = 'eq',
        ne = 'ne',
        gt = 'gt',
        gte = 'gte',
        lt = 'lt',
        lte = 'lte',
        in = 'in',
        nin = 'nin',
        regex = 'regex',
        not = 'not',
        nor = 'nor',
        like = 'like',
    }

    export interface FieldFilter {
        eq?: any;
        ne?: any;
        gt?: any;
        gte?: any;
        lt?: any;
        lte?: any;
        in?: any;
        nin?: any;
        regex?: any;
    }

    export enum LogicFilterOperator {
        or = 'or',
        and = 'and',
    }

    export interface LogicFilter {
        or?: QueryFilter;
        and?: QueryFilter;
        not?: QueryFilter;
        nor?: QueryFilter;
    }

    export interface QueryFilter extends LogicFilter {
        [fieldName: string]: QueryFilter | QueryFilter[] | FieldFilter | LogicFilter;
    }

    export interface IPaginationArguments {
        first?: number;
        last?: number;
        before?: string;
        after?: string;
        sort?: {
            [fieldName: string]: SortType;
        }[];
    }

    export interface IMultipleResolver extends IPaginationArguments {
        filter?: QueryFilter;
    }

    export interface RelayPagination<T> {
        edges?: { cursor: string; node: T }[];
        pageInfo?: {
            endCursor?: string;
            hasNextPage: boolean;
            hasPreviousPage: boolean;
            startCursor?: string;
            count?: number;
        };
    }
}

export namespace Generator {
    export enum IBasicResolverNames {
        create = 'create',
        update = 'update',
        remove = 'remove',
    }

    export interface IBasicResolver<T = any> {
        create?: ResolverFn<T>;
        update?: ResolverFn<T>;
        remove?: ResolverFn<T>;
    }

    export type hookFn = (
        entity: any,
        rootValue: any,
        args: any,
        context: any,
        info: GraphQLResolveInfo,
    ) => void | Promise<void>;

    export type mapperFn = (entity: any, field: string, value: any) => any;

    export interface IMutatorOptions {
        keyBlackList: string[];
        mapper?: (entity: any, field: string, value: any) => any;
    }

    export type mutatorFn = (
        model: Function,
        entity: any,
        args: object,
        options: IMutatorOptions,
    ) => any | Promise<any>;

    export interface ICUDOptions {
        hooks?: {
            preCreate?: hookFn;
            preUpdate?: hookFn;
            preRemove?: hookFn;
            postCreate?: hookFn;
            postUpdate?: hookFn;
            postRemove?: hookFn;
        };
        mutators: {
            create: {
                fn: mutatorFn;
                options?: IMutatorOptions;
            };
            update: {
                fn: mutatorFn;
                options?: IMutatorOptions;
            };
        };
        mappers: {
            create: mapperFn;
            update: mapperFn;
        };
    }
}

export namespace Filter {
    export type FilterParent = {
        filters: Query.QueryFilter;
        name: string;
    };

    export interface IFilterContext extends IContext {
        queryBuilder: SelectQueryBuilder<BaseEntity>;
    }
}

export interface ILogin {
    email: string;
    password: string;
}

export type GoogleProfile = {
    id: string;
    displayName: string;
    emails: [
        {
            value: string;
            type: string;
        }
    ];
    name: {
        familyName: string;
        givenName: string;
    };
    photos: { value: string }[];
    gender: string;
    provider: string;
    _raw: string;
    _json: {
        kind: string;
        etag: string;
        occupation: string;
        gender: string;
        urls: [
            {
                value: string;
                type: string;
                label: string;
            }
        ];
        objectType: string;
        id: string;
        displayName: string;
        name: {
            familyName: string;
            givenName: string;
        };
        url: string;
        image: {
            url: string;
            isDefault: true;
        };
        organizations: [
            {
                name: string;
                title: string;
                type: string;
                startDate: string;
                primary: true;
            }
        ];
        placesLived: [
            {
                value: string;
                primary: true;
            }
        ];
        isPlusUser: true;
        language: string;
        circledByCount: number;
        verified: false;
    };
};
