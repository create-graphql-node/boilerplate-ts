# GraphQL Starter

[![pipeline status](https://gitlab.com/TimKolb/create-graphql-node/boilerplate-ts/badges/master/pipeline.svg)](https://gitlab.com/TimKolb/create-graphql-node/boilerplate-ts/commits/master)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)

## How to use this blueprint

1.  🛠 Run the setup script
1.  ✏️ Extend graphql schema in separated graphql files
1.  ☁ Deploy mocked schema
1.  ✏️ Implement models and resolver functions
1.  ❌ Remove mocked resolvers
1.  ⛅ Deploy to production

## Setup

Run the setup script

```bash
$ ./cli setup
usage: cli [options] [flags]
  options:
    g|generate
      r|resolver [Entity]         generate resolver for entity

    s|setup                       install dependencies and configure development environment
        --clean                   clean interactively all files not tracked by git !CAUTION!
        --force                   force file override
        --debug                   enable debug mode for this script

  flags:
    --help                        display this help
```

and start the application

```bash
docker-compose up
```

### Development Flow

1.  Create a new GraphQL type.
1.  Add a query field
1.  Add create, update and delete mutation to the Mutation type

```graphql
type Query {
    # ...
    post(id: ID!): Post
}

type Mutation {
    # ...
    createPost(post: PostInput!): Post!
    updatePost(id: ID!, post: PostInput!): Post!
    removePost(id: ID!): Post!
}

type Post {
    id: ID!
    content: String!
    owner: User!
}

input PostInput {
    content: String!
}
```

**At this point the API is mocked and can be used**

You can visualize it with GraphQL Voyager:

-   visit Voyager Mutation http://localhost:3000/voyager/m
-   visit Voyager Query http://localhost:3000/voyager/q

or run queries:

-   visit GraphiQL http://localhost:3000/graphql

Create a corresponding TypeORM Model in `<root>/src/entity/${modelName}.ts`

Update the database schema with:

```bash
docker-compose run --rm app npm run sync
```

Create a new nested resolver `<root>/src/resolver/${modelName}/resolvers.ts` and add it to the main resolver in `<root>/src/resolver/resolvers.ts`

All resolvers will be merged and create an executable GraphQL schema.

```js
export const someResolver: NestedResolver = {
    Query: {
        post: async (rootValue: any, args: any, context: IContext) => {
            return context.connection.getRepository('Post').findOne(args.id);
        },
    },
    Mutation: {
        ...compose(
            withName('Post'),
            // just([IBasicResolverNames.remove, IBasicResolverNames.create])
        )(generateMutations(Post)),
        hello: async (rootValue: any, args: any, context: IContext) => {
            return `Hello back ${args.name}`;
        },
    },
};
```

### Authentication

Secure any field with provided directive `authorization`

```graphql
directive @auth(requires: Role = USER) on OBJECT | FIELD_DEFINITION

type Query {
    me: ID @auth
    post: Post @auth(requires: "ADMIN")
}
```

**Want to check for entity ownership?**

```js
generateMutations(Model, { hooks: {
    preUpdate: (entity, rootValue, args, context, info) => {
        if (context.user.id !== entity.ownerField) {
            throw new Error(`You cannot update this ${Model.name}. You are not the owner.`);
        }
    }
})
```

## Purpose of this project

This project provides a opinionated but flexible codebase to speed up API development time

The project goal is to enable a developer to:

-   Define entities in GraphQL language and generate TypeORM Typescript files
-   Apply generic filters on any type
-   Generate create, update, delete mutations

## Code Intro

### Predefined GraphQL types

```graphql
scalar DateTime

scalar Cursor

input BooleanFilter {
    eq: Boolean
}

input StringFilter {
    eq: String
    like: String
    ne: String
    regex: String
}

input IntegerFilter {
    eq: Int
    ne: Int
    gt: Int
    gte: Int
    lt: Int
    lte: Int
    in: [Int]
    nin: [Int]
}

enum SortDirection {
    ASC
    DESC
}

interface Node {
    id: ID!
}

type PageInfo {
    startCursor: Cursor
    endCursor: Cursor
    hasPreviousPage: Boolean
    hasNextPage: Boolean
    count: Int!
}
```

### Mutations

```graphql
type Mutation {
    createPost(post: { content: String!, likes: Int, done: Boolean }): Post!
    updatePost(id: ID!, post: { content: String! }): Post!
    removePost(id: ID!): Post!
}
```

```js
// src/resolver/post/index.ts
const postResolver: NestedResolver = {
    Mutation: {
        ...compose(
            withName('Post'),
            just([IBasicResolverNames.remove, IBasicResolverNames.create]),
        )(generateMutations(Post)),
    },
    // ...
};
```

### Queries

```graphql
type Query {
    allPosts(
        first: Int
        last: Int
        after: Cursor
        before: Cursor
        filter: PostFilter
        sort: [Sort]
    ): PaginatedPosts!
}

input PostFilter {
    or: [PostFilter!]
    and: [PostFilter!]
    id: IntegerFilter
    content: StringFilter
    done: BooleanFilter
    likes: IntegerFilter
}
```

Each query functionality mutates a optional querybuilder with name 'base' according to the request. Pagination is the last transformer, which returns the entities from the database

```js
// src/resolver/post/index.ts
const postResolver: NestedResolver = {
    // ...
    Query: {
        allPosts: async (rootValue, args, context: IContext, info) => {
            const repository = context.connection.getRepository(Post);
            const queryBuilder = repository.createQueryBuilder();

            filter(rootValue, args, { ...context, queryBuilder }, info);

            return paginate({
                repository,
                resolver: {
                    rootValue,
                    args,
                    context,
                    info,
                },
                queryBuilder,
            });
        },
    },
};
```

Allows to run queries like:

```graphql
{
    allPosts(
        filter: {
            or: [{ content: { like: "b" }, done: { eq: false } }, { content: { like: "a" } }]
            and: { likes: { lte: 13 } }
        }
    ) {
        edges {
            node {
                id
            }
        }
    }
}
```

## License

MIT

## Author

Tim Kolberger
[<img src="https://simpleicons.org/icons/gmail.svg" width="24"/>](mailto:tim@kolberger.eu)
[<img src="https://simpleicons.org/icons/twitter.svg" width="24"/>](https://twitter.com/TimKolberger)
