#!/usr/bin/env bash

set -e

# General functions
BOLD_TEXT=$(tput bold)
NORMAL_TEXT=$(tput sgr0)
YELLOW='\033[1;33m' # Yellow text color
RED='\033[0;31m'    # Red text color
GREEN='\033[0;32m'    # Green text color
BLUE='\033[0;34m'    # Blue text color
NC='\033[0m'        # No text color

customPrint () {
    echo "######################################################"
    echo "# ${BOLD_TEXT}${@}"${NORMAL_TEXT}
    echo "######################################################"
}

printSuccess() {
   echo -e "✅  ${GREEN}${@}${NC}"
}

printInfo() {
   echo -e "☝️  ${BLUE}${@}${NC}"
}

printWarning() {
   echo -e "⚠️  ${YELLOW}${@}${NC}"
}

printError() {
   echo -e "🛑  ${RED}${@}${NC}"
}

fileExists() {
    if [ ! -f $1 ] || [ ${FORCE} == 'YES' ]; then
        return 1
    else
        return 0
    fi
}



# Get input flags
CLEAN=NO
DEBUG=NO
FORCE=NO
POSITIONAL=()

while [[ $# -gt 0 ]]; do
    key="$1"
    case ${key} in
        --debug)
        DEBUG=YES
        shift # past argument
        ;;

        --force)
        FORCE=YES
        shift # past argument
        ;;

        --clean)
        CLEAN=YES
        shift # past argument
        ;;

        *)    # unknown option
        POSITIONAL+=("$1") # save it in an array for later
        shift # past argument
        ;;
    esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

if [ "${1}" == "r" ] || [ "${1}" == "resolver" ]; then
 NAME="$2"
 if [ -z "$NAME" ]; then
    printError "Please provide a name for the new resolver"
    exit 1
 fi

 LOWERCASE_NAME=$(echo "$NAME" | tr '[:upper:]' '[:lower:]')
 RESOLVER_DIR=src/schema/${LOWERCASE_NAME}
 RESOLVER_FILE_PATH=$RESOLVER_DIR/index.ts
 mkdir -p $RESOLVER_DIR

 if fileExists $RESOLVER_FILE_PATH; then
    printError "$RESOLVER_FILE_PATH exists. Skipping! Use --force to override"
    exit 1
 fi

 sed -e "s/\$NAME/$NAME/g" \
     -e "s/\$LOWERCASE_NAME/$LOWERCASE_NAME/g" \
     -e "s/\$unconfigured//g" .development/blueprints/resolver.ts > $RESOLVER_FILE_PATH

 printSuccess "Created resolver for $NAME in $RESOLVER_FILE_PATH"
fi

if [ ${DEBUG} == "YES" ]; then
    set -x
    printWarning "Debug mode is enabled"
fi

if [ ${CLEAN} == "YES" ]; then
    # interactive git clean
    git clean -xdi
    printSuccess "DONE"
    exit
fi

exit 1