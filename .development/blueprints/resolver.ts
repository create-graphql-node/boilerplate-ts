import { IContext } from '../../index';
import { NestedResolver } from '../../types/types';
import { $NAME } from '../../entity/$NAME';
import {
    compose,
    generateMutations,
    withName,
} from '../../lib/generateMutations';

const $LOWERCASE_NAMEResolver: NestedResolver = {
    Mutation: {
        ...compose(withName('$NAME'))(generateMutations($NAME)),
        customField: async (_: any, args: any, context: IContext): Promise<any> => {
            return null;
        },
    },
};

export default $LOWERCASE_NAMEResolver;
