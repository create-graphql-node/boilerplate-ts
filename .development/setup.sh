#!/usr/bin/env bash

set -e

# General functions
BOLD_TEXT=$(tput bold)
NORMAL_TEXT=$(tput sgr0)
YELLOW='\033[1;33m' # Yellow text color
RED='\033[0;31m'    # Red text color
GREEN='\033[0;32m'    # Green text color
BLUE='\033[0;34m'    # Blue text color
NC='\033[0m'        # No text color

customPrint () {
    echo "######################################################"
    echo "# ${BOLD_TEXT}${@}"${NORMAL_TEXT}
    echo "######################################################"
}

printSuccess () {
   echo -e "${GREEN}${@}${NC}"
}

printInfo () {
   echo -e "${BLUE}${@}${NC}"
}

printWarning () {
   echo -e "${YELLOW}${@}${NC}"
}

printError () {
   echo -e "${RED}${@}${NC}"
}

fileDoesNotExists () {
    if [ ! -f $1 ] || [ ${FORCE} == 'YES' ]; then
        return 0
    else
        return 1
    fi
}

setup() {
    if fileDoesNotExists .env; then
        cp .env.dist .env
    else
        printWarning "Skipping copy .env.dist to .env. File exists!"
    fi

    printInfo "Install dependencies"
    docker-compose run --rm app npm install

    printInfo "Update the database schema"
    docker-compose up -d database
    docker-compose run --rm app npm run sync
    docker-compose stop database

    if [ -z "$PS1" ]; then
            printSuccess "DONE"
    else
        read -p "Start the application? [Y/n] " choice
        case "$choice" in
            y|Y|yes)
            docker-compose up -d && docker-compose logs -f
            ;;

            n|N|no)
            exit
            ;;

            *)
            docker-compose up -d && docker-compose logs -f
            ;;
        esac
    fi
}

# Get input flags
CLEAN=NO
DEBUG=NO
FORCE=NO
POSITIONAL=()

while [[ $# -gt 0 ]]; do
    key="$1"
    case ${key} in
        --debug)
        DEBUG=YES
        shift # past argument
        ;;

        --force)
        FORCE=YES
        shift # past argument
        ;;

        --clean)
        CLEAN=YES
        shift # past argument
        ;;

        *)    # unknown option
        POSITIONAL+=("$1") # save it in an array for later
        shift # past argument
        ;;
    esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

if [ ${DEBUG} == "YES" ]; then
    set -x
    printWarning "Debug mode is enabled"
fi

if [ ${CLEAN} == "YES" ]; then
    # interactive git clean
    git clean -xdi
    printSuccess "DONE"
    exit
fi

setup